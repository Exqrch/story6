from django.shortcuts import render, redirect
from .models import Matakuliah
from .forms import FormMatakuliah

# Create your views here.
def cover(request) :
	return render(request,'mainapp/cover.html')

def index(request) :
	return render(request,'mainapp/index.html')

def contact(request) :
	return render(request,'mainapp/contact.html')

def matakuliah_detail_view(request) :
	obj = Matakuliah.objects.all().count()
	temp = 1
	lst = list(Matakuliah.objects.all())

	context = {
		"object" : lst
	}
	return render(request, "mainapp/matkuldetail.html", context)

def add_matkul(request) : 
	form = FormMatakuliah()

	if request.method == "POST" :
		form = FormMatakuliah(request.POST)
		if form.is_valid():
			Matkul = Matakuliah()
			Matkul.nama_matkul 	= form.cleaned_data['nama_matkul']
			Matkul.dosen 		= form.cleaned_data['dosen']
			Matkul.sks 			= form.cleaned_data['sks']
			Matkul.ruang 		= form.cleaned_data['ruang']
			Matkul.tahun_ajaran	= form.cleaned_data['tahun_ajaran']
			Matkul.deskripsi 	= form.cleaned_data['deskripsi']
			Matkul.save()
			form = FormMatakuliah()
			return render(request, "mainapp/add_matkul.html", {'form' : form, 'flag' : 1})
		else :
			return render(request, "mainapp/add_matkul.html", {'form' : form, 'flag' : 0})
	else :
		return render(request, "mainapp/add_matkul.html", {'form' : form})

def detail(request, pk):
	obj = Matakuliah.objects.get(pk=pk)
	context = {'obj': obj}
	return render(request, 'mainapp/matkuldesc.html', context)

def delete(request, pk):
	obj = Matakuliah.objects.get(pk=pk)
	obj.delete()
	return redirect('matkuldetail')
