from django.db import models

# Create your models here.
class Matakuliah(models.Model):
	nama_matkul = models.CharField(max_length=40)
	dosen 		= models.CharField(max_length=40)
	sks 		= models.IntegerField()
	ruang 		= models.CharField(max_length=8)
	tahun_ajaran= models.CharField(max_length=16)
	deskripsi 	= models.TextField(blank=True, null=True)
"""blank=False --> Required, null=False --> It's ok for it to be empty in database"""
