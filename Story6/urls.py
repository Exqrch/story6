"""Story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story1.views import story1html
from story3.views import story3index, story3org, story3hobbies, story3achievement
from mainapp.views import index, contact, cover, matakuliah_detail_view, add_matkul, detail, delete
from kegiatan.views import index_kegiatan
from peserta_kegiatan.views import index_peserta

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', cover, name="cover"),
    path('index/', index, name="index"),
    path('index/contact/', contact, name="contact"),
    path('index/matkuldetail/', matakuliah_detail_view, name="matkuldetail"),
    path('index/matkuldetail/<str:pk>', detail, name="detail"),
    path('index/matkuldetail/delete/<str:pk>', delete, name="delete"),
    path('index/tambah_matkul/', add_matkul, name="addmatkul"),
    path('story1/', story1html, name="story1"),
    path('story3/index/', story3index, name="story3index"),
    path('story3/org/', story3org, name="story3org"),
    path('story3/hobbies/', story3hobbies, name="story3hobbies"),
    path('story3/achievement/', story3achievement, name="story3achievement"),
    path('index/register/', index_peserta, name="register_peserta"),
    path('index/kegiatan/', index_kegiatan, name="kegiatan"),
]