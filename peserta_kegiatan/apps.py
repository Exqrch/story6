from django.apps import AppConfig


class PesertaKegiatanConfig(AppConfig):
    name = 'peserta_kegiatan'
