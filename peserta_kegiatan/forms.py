from django import forms
from .models import Peserta

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = "__all__"
        widgets = {
            'kegiatan' : forms.Select(attrs={'class': 'form-control'}),
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
        }