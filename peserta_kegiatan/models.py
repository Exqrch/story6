from django.db import models
from kegiatan.models import Kegiatan

# Create your models here.
class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.CASCADE)
    name = models.CharField(max_length=30)