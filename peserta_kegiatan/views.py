from django.shortcuts import render, redirect   
from .forms import PesertaForm
from .models import Peserta

# Create your views here.

def index_peserta(request):
    if(request.method == "POST"):
        form = PesertaForm(request.POST)
        if(form.is_valid()):
            form.save()
        return redirect("/index/kegiatan")
    else:
        form = PesertaForm()
        peserta = Peserta.objects.all()
        context = {
            'form' : form,
            'peserta' : peserta
        }
        return render(request, 'peserta_kegiatan/peserta.html', context)
