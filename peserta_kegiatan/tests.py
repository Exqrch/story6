from django.test import TestCase,Client
from django.urls import resolve
from .views import index_peserta
from .models import Peserta
from kegiatan.models import Kegiatan
# Create your tests here
class unitTest(TestCase):
    def test_app_url_exist(self):
        response=Client().get('/index/register/')
        self.assertEqual(response.status_code,200)
    def test_app_func(self):
        found=resolve('/index/register/')
        self.assertEqual(found.func, index_peserta)
    def test_model_cek(self):
        Kegiatan.objects.create(name="Main Bareng", description="Skuy bersenang-senang")
        Peserta.objects.create(kegiatan=Kegiatan.objects.get(name="Main Bareng"), name="Lucky")
        hitungjumlah = Peserta.objects.all().count()
        self.assertEqual(hitungjumlah,1)
    def test_using_right_template(self):
        response=Client().get('/index/register/')
        self.assertTemplateUsed(response, 'peserta_kegiatan/peserta.html')
