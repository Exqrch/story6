from django import forms
from .models import Kegiatan

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
            'description' : forms.TextInput(attrs={'class': 'form-control'}),
        }

