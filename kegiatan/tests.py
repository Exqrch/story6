from django.test import TestCase,Client
from django.urls import resolve
from .views import index_kegiatan
from .models import Kegiatan
# Create your tests here
class unitTest(TestCase):
    def test_app_url_exist(self):
        response=Client().get('/index/kegiatan/')
        self.assertEqual(response.status_code,200)
        
    def test_app_func(self):
        found=resolve('/index/kegiatan/')
        self.assertEqual(found.func, index_kegiatan)
        
    def test_model_cek(self):
        Kegiatan.objects.create(name="Main Game", description="Mari kita bersenang-senang")
        hitungjumlah = Kegiatan.objects.all().count()
        self.assertEqual(hitungjumlah,1)
        
    def test_using_right_template(self):
        response=Client().get('/index/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')
