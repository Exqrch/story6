from django.shortcuts import render, redirect
from .forms import KegiatanForm
from .models import Kegiatan
from peserta_kegiatan.models import Peserta

# Create your views here.
def index_kegiatan(request):
    if(request.method == "POST"):
        form = KegiatanForm(request.POST)
        if(form.is_valid()):
            form.save()
        return redirect('/index/kegiatan')
    else:
        form = KegiatanForm()
        kegiatan = Kegiatan.objects.all()
        peserta = Peserta.objects.all()
        context = {
            'form' : form,
            'kegiatan' : kegiatan,
            'peserta' : peserta
        }
        return render(request, 'kegiatan/kegiatan.html', context)

